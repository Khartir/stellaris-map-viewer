const fs = require('fs');
// const { parse } = require('jomini');
// const raw = fs.readFileSync('./resources/gamestate', 'utf8');
// const parsedRaw = parse(raw);
// fs.writeFileSync('./resources/gamestate.json', JSON.stringify(parsedRaw));
// const file = fs.readFileSync('./resources/gamestate.json', 'utf8');
const file = fs.readFileSync('./resources/extended.json', 'utf8');
const {
    // species,
    // clusters,
    country,
    galactic_object,
    galaxy,
    galaxy_radius,
    // planets,
    starbases,
    // used_color,
    player
} = JSON.parse(file);
const parsed = {
    systems: {},
    countries: {},
    hyperlanes: {},
    galaxy: { outerRadius: galaxy_radius, innerRadius: galaxy.core_radius },
    players: player
};
for (const [
    index,
    {
        coordinate: { x, y },
        type,
        name,
        hyperlane
    }
] of Object.entries(galactic_object)) {
    const hyperlanes = [];
    if (Array.isArray(hyperlane)) {
        for (const lane of hyperlane) {
            const systems = [lane.to, parseInt(index)];
            systems.sort();
            parsed.hyperlanes[systems.join('-')] = { systems, length: lane.length };
            hyperlanes.push(systems.join('-'));
        }
    }
    parsed.systems[index] = {
        coordinate:
            //convert stellaris to canvas coords
            { x: -x, y },
        type,
        name,
        hyperlanes,
        owner: undefined
    };
}
for (const [, { system, owner }] of Object.entries(starbases)) {
    parsed.systems[system].owner = owner;
}
for (const [index, { flag, name, starting_system, terra_incognita, visited_objects, surveyed }] of Object.entries(
    country
)) {
    if (!flag) {
        continue;
    }
    parsed.countries[index] = {
        color: flag.colors[0],
        name,
        starting_system,
        terra_incognita,
        visited_objects,
        surveyed
    };
}
fs.writeFileSync('./src/resources/extended.js', 'export const state = ' + JSON.stringify(parsed));
// fs.writeFileSync('./src/resources/basic.js', 'export const state = ' + JSON.stringify(parsed));
