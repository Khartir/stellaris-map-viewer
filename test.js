const fs = require('fs');
const file = fs.readFileSync('./resources/gamestate.json', 'utf8');

const raw = JSON.parse(file);

/*
console.log(Object.keys(raw));
[
'version',
'version_control_revision',
'name',
'date',
'required_dlcs',
'player',
'tick',
'random_log_day',
'species',
'last_created_species',
'nebula',
'pop',
'last_created_pop',
'galactic_object',
'starbases',
'planets',
'country',
'alliance',
'truce',
'trade_deal',
'last_created_country',
'last_refugee_country',
'last_created_system',
'leaders',
'ships',
'fleet',
'fleet_template',
'last_created_fleet',
'last_created_ship',
'last_created_leader',
'last_created_army',
'last_created_design',
'army',
'deposit',
'ground_combat',
'fired_events',
'war',
'debris',
'missile',
'strike_craft',
'ambient_object',
'last_created_ambient_object',
'message',
'last_diplo_action_id',
'last_notification_id',
'last_event_id',
'random_name_database',
'name_list',
'galaxy',
'galaxy_radius',
'flags',
'saved_event_target',
'ship_design',
'pop_factions',
'last_created_pop_faction',
'last_killed_country_name',
'megastructures',
'bypasses',
'natural_wormholes',
'trade_routes',
'sectors',
'buildings',
'archaeological_sites',
'global_ship_design',
'clusters',
'rim_galactic_objects',
'used_color',
'used_symbols',
'used_species_names',
'used_species_portrait',
'random_seed',
'random_count',
'market',
'trade_routes_manager',
'slave_market_manager'
]
*/

/*
console.log(raw.player);
[ { name: 'Dorja', country: 0 } ]
*/

// console.log(raw.country["0"]);
/*
terra_incognita
visited_objects
surveyed
starting_system
*/

console.log(raw.starbases["4294967295"]);