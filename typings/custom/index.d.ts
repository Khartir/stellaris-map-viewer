declare module 'worker-loader!../worker/loader' {
    class WebpackWorker extends Worker {
        constructor();
    }

    export default WebpackWorker;
}
