# Stellaris map viewer

A webapp to read [Stellaris](https://www.paradoxplaza.com/stellaris/STST01G-MASTER.html) save files and render an editable map.

## Running app

https://khartir.gitlab.io/stellaris-map-viewer/
