import { Voronoi } from 'd3-delaunay';
import { Polygon, Ring } from 'polygon-clipping';
import React, { useMemo } from 'react';
import { Circle, Line } from 'react-konva';

import { getEmpires } from './empires';
import { getColor } from './helpers';
import { useGalaxy } from './hooks/useGalaxy';

export function Helpers({ voroni = false }) {
    const [state] = useGalaxy();
    let v: Voronoi<number[]> | null = null;
    if (voroni) {
        v = getEmpires(state);
        v.xmax = state.galaxy.outerRadius + 10;
        v.xmin = -state.galaxy.outerRadius - 10;
        v.ymax = state.galaxy.outerRadius + 10;
        v.ymin = -state.galaxy.outerRadius - 10;
    }
    const Systems = useMemo(() => {
        if (v === null) {
            return [];
        }
        const ownedSystems = Object.values(state.systems).reduce((result, system, index) => {
            if (!system.owner) {
                return result;
            }
            // if (system.owner !== 8) {
            //     return result;
            // }
            // if (index !== 26) {
            //     return result;
            // }
            //@ts-ignore
            const cell = (v.cellPolygon(index) as unknown) as Ring;
            if (!cell) {
                console.log(index, cell);
                return result;
            }
            if (!result[system.owner]) {
                result[system.owner] = [];
            }
            result[system.owner].push(cell);
            return result;
        }, [] as Polygon[]);

        const Systems = ownedSystems.reduce((result, systems, owner) => {
            systems.forEach((points, index) => {
                const line = (
                    <Line
                        points={points.flat(2)}
                        stroke={getColor(owner, state)}
                        closed
                        // tension={0.3}
                        key={`${owner}-${index}`}
                    />
                );
                result.push(line);
            });
            return result;
        }, [] as JSX.Element[]);

        return Systems;
    }, [state, v]);

    return (
        <>
            {Systems}
            <Circle x={0} y={0} radius={state.galaxy.innerRadius - 5} stroke="white" strokeWidth={1} />
            <Circle x={0} y={0} radius={state.galaxy.outerRadius + 5} stroke="white" strokeWidth={1} />
        </>
    );
}
