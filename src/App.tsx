import Konva from 'konva';
import React, { useEffect, useRef } from 'react';
import { Layer, Stage } from 'react-konva';

import { Empires } from './Empires';
import { Helpers } from './Helpers';
import { useZoom } from './helpers';
import { ConfigProvider, isProduction, useConfig } from './hooks/useConfig';
import { GalaxyProvider, useExampleSave, useGalaxy } from './hooks/useGalaxy';
import { Sidebar } from './sidebar/Sidebar';
import { Systems } from './Systems';

let resizeTimer: NodeJS.Timeout;

const scaleBy = 2;
export function App() {
    return (
        <ConfigProvider>
            <GalaxyProvider>
                <WrappedApp />
            </GalaxyProvider>
        </ConfigProvider>
    );
}

function WrappedApp() {
    const stage = useRef<Stage>(null);
    const zoom = useZoom(stage);
    const container = useRef<HTMLDivElement>(null);
    const [config, configAction] = useConfig();
    const setExampleSave = useExampleSave();
    useEffect(() => {
        if (!isProduction) {
            setExampleSave();
        }
    }, [setExampleSave]);

    const galaxy = useGalaxy();
    useEffect(() => {
        function onResize() {
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(function() {
                if (container.current) {
                    const newSize = {
                        width: container.current.offsetWidth,
                        height: window.innerHeight - 16
                    };
                    configAction.setCanvasSize(newSize);
                }
            }, 250);
        }
        if (container.current) {
            const newSize = { width: container.current.offsetWidth, height: window.innerHeight - 16 };
            configAction.setCanvasSize(newSize);
        }
        window.addEventListener('resize', onResize);
        return () => {
            window.removeEventListener('resize', onResize);
        };
    }, [configAction]);

    useEffect(() => {
        configAction.setOffset({
            x: -((stage.current && config.canvasSize.width / 2) || 0),
            y: -((stage.current && config.canvasSize.height / 2) || 0)
        });
    }, [config.canvasSize.height, config.canvasSize.width, configAction]);

    function onWheel(e: Konva.KonvaEventObject<WheelEvent>) {
        if (!e.target || !stage.current) {
            return;
        }
        e.evt.preventDefault();
        const pointerPosition = stage.current.getStage().getPointerPosition();
        if (!pointerPosition) {
            return;
        }

        const newScale = e.evt.deltaY > 0 ? config.scale * scaleBy : config.scale / scaleBy;
        zoom(newScale, pointerPosition);
    }

    return (
        <div style={{ display: 'flex' }}>
            <div style={{ width: '20vw' }}>
                <Sidebar stage={stage}></Sidebar>
            </div>
            <div ref={container} style={{ backgroundColor: '#e3e3e3', width: '80vw' }}>
                <Stage
                    ref={stage}
                    width={config.canvasSize.width}
                    height={config.canvasSize.height}
                    onwheel={onWheel}
                    draggable
                >
                    {config.offset.x !== 0 && (
                        <ConfigProvider state={[config, configAction]}>
                            <GalaxyProvider state={galaxy}>
                                {config.displayHelpers && (
                                    <Layer offset={config.offset} scale={{ x: config.scale, y: config.scale }}>
                                        <Helpers voroni={false} />
                                    </Layer>
                                )}
                                <Layer offset={config.offset} scale={{ x: config.scale, y: config.scale }}>
                                    <Empires />
                                </Layer>
                                <Layer offset={config.offset} scale={{ x: config.scale, y: config.scale }}>
                                    <Systems />
                                </Layer>
                            </GalaxyProvider>
                        </ConfigProvider>
                    )}
                </Stage>
            </div>
        </div>
    );
}
