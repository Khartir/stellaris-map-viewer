import React, { createContext, ReactNode } from 'react';
import { DispatcherMap, useLocalSlice } from 'use-local-slice';

import { Country, System } from './useGalaxy';

interface Config {
    scale: number;
    canvasSize: { width: number; height: number };
    offset: { x: number; y: number };
    displayHelpers: boolean;
    displayStarNames: boolean;
    hyperlaneColor: string;
    draggableStars: boolean;
    spoilerFree: boolean;
    activePlayer: number | undefined;
    selectedObject: System | Country | undefined;
    systemFontSize: number;
}
type Setter<T> = (state: Config, { payload }: { payload: T }) => void;
type Map = {
    setScale: Setter<number>;
    setCanvasSize: Setter<Config['canvasSize']>;
    setOffset: Setter<Config['offset']>;
    setDisplayHelpers: Setter<boolean>;
    setDisplayStarNames: Setter<boolean>;
    setHyperlaneColor: Setter<string>;
    setStarsDraggable: Setter<boolean>;
    setSpoilerFree: Setter<boolean>;
    setActivePlayer: Setter<number>;
    setSelectedObject: Setter<System | Country | undefined>;
    setSystemFontSize: Setter<number>;
};

const context = createContext<[Config, DispatcherMap<Map>] | undefined>(undefined);

export const isProduction = process.env.NODE_ENV === 'production';

export function ConfigProvider({
    state,
    children
}: {
    state?: [Config, DispatcherMap<Map>] | undefined;
    children: ReactNode;
}) {
    const defaultState = useLocalSlice<Config, Map>({
        initialState: {
            scale: 1,
            canvasSize: { width: 0, height: 0 },
            displayHelpers: false,
            displayStarNames: false,
            hyperlaneColor: 'rgba(255,255,255,0.5)',
            draggableStars: false,
            spoilerFree: isProduction,
            activePlayer: undefined,
            selectedObject: undefined,
            systemFontSize: 14,
            offset: { x: 0, y: 0 }
        },
        reducers: {
            setScale: (state, { payload }) => {
                state.scale = payload;
            },
            setCanvasSize: (state, { payload }) => {
                state.canvasSize = payload;
            },
            setOffset: (state, { payload }) => {
                state.offset = payload;
            },
            setDisplayHelpers: (state, { payload }) => {
                state.displayHelpers = payload;
            },
            setDisplayStarNames: (state, { payload }) => {
                state.displayStarNames = payload;
            },
            setHyperlaneColor: (state, { payload }) => {
                state.hyperlaneColor = payload;
            },
            setStarsDraggable: (state, { payload }) => {
                state.draggableStars = payload;
            },
            setSpoilerFree: (state, { payload }) => {
                state.spoilerFree = payload;
            },
            setActivePlayer: (state, { payload }) => {
                state.activePlayer = payload;
            },
            setSelectedObject: (state, { payload }) => {
                state.selectedObject = payload;
            },
            setSystemFontSize: (state, { payload }) => {
                state.systemFontSize = payload;
            }
        }
    });
    const { Provider } = context;

    return <Provider value={state || defaultState}>{children}</Provider>;
}
export function useConfig() {
    const consumed = React.useContext(context);
    if (!consumed) {
        throw new Error('useConfig can only be used within a ConfigProvider!');
    }
    return consumed;
}
