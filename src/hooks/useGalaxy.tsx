import Konva from 'konva';
import React, { createContext, ReactNode, useCallback, useContext } from 'react';
import { DispatcherMap, useLocalSlice } from 'use-local-slice';

import { getSystem } from '../helpers';
// import { state as exampleState } from '../resources/basic';
import { state as exampleState } from '../resources/extended';

import { useConfig } from './useConfig';

const initialState: State = {
    systems: {},
    countries: {},
    hyperlanes: {},
    galaxy: { outerRadius: 500, innerRadius: 100 },
    players: []
};

export interface System {
    coordinate: { x: number; y: number };
    type: string;
    name: string;
    hyperlanes: string[];
    owner?: number | undefined;
}

export interface Country {
    color: string;
    name: string;
    terra_incognita: { size: number; data: number[]; systems: number[] };
    visited_objects: number[];
    surveyed?: number[];
    starting_system?: number;
}
export interface State {
    systems: {
        [index: string]: System;
    };
    countries: {
        [index: string]: Country;
    };
    hyperlanes: {
        [name: string]: {
            systems: number[];
            length: number;
        };
    };
    galaxy: { outerRadius: number; innerRadius: number };
    players: { name: string; country: number }[];
}

type Map = {
    moveSystem: (state: State, { payload }: { payload: Konva.KonvaEventObject<MouseEvent> & { id: number } }) => void;
    switchSystemOwner: (state: State, { payload }: { payload: number }) => void;
    setGalaxy: (state: State, { payload }: { payload: State }) => void;
};

export function useExampleSave() {
    const setGalaxy = useSetGalaxy();
    return useCallback(() => setGalaxy(exampleState), [setGalaxy]);
}

export function useSetGalaxy() {
    const [, dispatch] = useGalaxy();
    const [, configAction] = useConfig();
    return useCallback(
        (galaxy: State) => {
            dispatch.setGalaxy(galaxy);
            if (galaxy.players.length) {
                configAction.setActivePlayer(galaxy.players[0].country);
            }
        },
        [configAction, dispatch]
    );
}

const context = createContext<[State, DispatcherMap<Map>] | undefined>(undefined);
export function GalaxyProvider({
    state,
    children
}: {
    state?: [State, DispatcherMap<Map>] | undefined;
    children?: ReactNode;
}) {
    const defaultState = useLocalSlice<State, Map>({
        initialState,
        reducers: {
            moveSystem: (state, action: { payload: Konva.KonvaEventObject<MouseEvent> & { id: number } }) => {
                const system = getSystem(action.payload.id, state);
                system.coordinate.x = action.payload.target.x();
                system.coordinate.y = action.payload.target.y();
            },
            switchSystemOwner: (state, action: { payload: number }) => {
                // const system = getSystem(action.payload, state);
                // system.owner === 3 ? (system.owner = 0) : (system.owner = 3);
            },
            setGalaxy: (state, { payload }) => {
                state.countries = payload.countries;
                state.galaxy = payload.galaxy;
                state.hyperlanes = payload.hyperlanes;
                state.systems = payload.systems;
                state.players = payload.players;
            }
        }
    });
    const { Provider } = context;

    return <Provider value={state || defaultState}>{children}</Provider>;
}

export function useGalaxy() {
    const consumed = useContext(context);
    if (!consumed) {
        throw new Error('useGalaxy can only be used within a GalaxyProvider!');
    }
    return consumed;
}
