import Flatten from 'flatten-js';
import Konva from 'konva';
import { Pair, Polygon, Ring, union } from 'polygon-clipping';
import React, { useMemo } from 'react';
import { Group, KonvaNodeEvents, Line } from 'react-konva';

import { getEmpires } from './empires';
import { getColor, useGetCountry } from './helpers';
import { useConfig } from './hooks/useConfig';
import { State, useGalaxy } from './hooks/useGalaxy';

const { Point, Line: HelperLine, Circle: HelperCircle } = Flatten;

export function Empires() {
    const [state] = useGalaxy();
    const [config, configAction] = useConfig();
    const v = useMemo(() => getEmpires(state), [state]);
    const getCountry = useGetCountry();

    const ownedSystems = useMemo(
        () =>
            Object.values(state.systems).reduce((result, system, index) => {
                if (system.owner === undefined) {
                    return result;
                }
                if (config.spoilerFree) {
                    if (config.activePlayer === undefined) {
                        return result;
                    }
                    const country = state.countries[config.activePlayer];
                    const surveyed = country.visited_objects || [];

                    if (system.owner !== config.activePlayer && !surveyed.includes(index)) {
                        return result;
                    }
                }
                const cell = v.cellPolygon(index);
                if (!cell) {
                    console.log(index, cell);
                    return result;
                }
                const points = restrictToGalaxy(cell, state.galaxy.innerRadius - 5, state.galaxy.outerRadius + 5);
                if (!result[system.owner]) {
                    result[system.owner] = [];
                }
                result[system.owner].push(points);
                return result;
            }, [] as Polygon[]),
        [
            config.activePlayer,
            config.spoilerFree,
            state.countries,
            state.galaxy.innerRadius,
            state.galaxy.outerRadius,
            state.systems,
            v
        ]
    );

    const groupProps = useMemo(() => {
        return ownedSystems.reduce((result, [firstSystem, ...systems], owner) => {
            const merged = union([firstSystem], ...systems.map(system => [system]));
            merged.forEach((shapes, index) => {
                const lines = [] as JSX.Element[];
                let params = {
                    hitStrokeWidth: 0,
                    shadowForStrokeEnabled: false
                } as Konva.LineConfig & KonvaNodeEvents;
                shapes.forEach((points, secIndex) => {
                    params.globalCompositeOperation = 'source-over';
                    params.stroke = getColor(owner, state);
                    if (0 === secIndex) {
                        params.fill = getTerritoryColor(owner, state);
                        params.onClick = () => configAction.setSelectedObject(getCountry(owner));
                    } else {
                        params.fill = 'white';
                    }
                    const stroke = (
                        <Line points={points.flat(1)} closed key={`${owner}-${index}-${secIndex}-stroke`} {...params} />
                    );
                    lines.push(stroke);
                    params.stroke = undefined;
                    if (0 < secIndex) {
                        params.globalCompositeOperation = 'destination-out';
                        const deleter = (
                            <Line
                                points={points.flat(1)}
                                closed
                                key={`${owner}-${index}-${secIndex}-fill`}
                                {...params}
                            />
                        );
                        lines.push(deleter);
                    }
                });
                if (lines.length > 0) {
                    params.fill = 'white';
                    params.globalCompositeOperation = 'destination-in';
                    const fill = (
                        <Line points={shapes[0].flat(1)} closed key={`${owner}-${index}-0-fill`} {...params} />
                    );
                    lines.push(fill);
                }
                const groupProps = {
                    key: `${owner}-${index}`,
                    children: lines
                };
                result.push(groupProps);
            });
            return result;
        }, [] as { key: string; children: JSX.Element[] }[]);
    }, [ownedSystems, state, configAction, getCountry]);

    return (
        <>
            {groupProps.reduce((result, { key, children }) => {
                result.push(
                    <Group
                        ref={group => {
                            if (!group) {
                                return;
                            }
                            group.cache({ pixelRatio: config.scale });
                        }}
                        key={key}
                    >
                        {children}
                    </Group>
                );
                return result;
            }, [] as JSX.Element[])}
        </>
    );
}

function restrictToGalaxy(points: number[][], innerRadius: number, outerRadius: number): Ring {
    if (JSON.stringify(points[0]) === JSON.stringify(points[points.length - 1])) {
        //last point = first point
        points.splice(points.length - 1, 1);
    }
    return points.reduce((result, [x, y], index) => {
        const d = getDistance(x, y);
        if (isInside(d, innerRadius, outerRadius)) {
            result.push([x, y]);
        } else {
            const previousPoint = points[(index - 1 + points.length) % points.length];
            const projection = getProjection(previousPoint, x, y, outerRadius, innerRadius, d);
            if (projection) {
                result.push(projection);
            }
            const nextPoint = points[(index + 1) % points.length];

            const projection2 = getProjection(nextPoint, x, y, outerRadius, innerRadius, d);
            if (projection2) {
                result.push(projection2);
            }
        }

        return result;
    }, [] as Pair[]);
}

function getProjection(
    otherPoint: number[],
    x: number,
    y: number,
    outerRadius: number,
    innerRadius: number,
    d: number
): Pair | undefined {
    const originalPoint = new Point(x, y);
    const [x0, y0] = otherPoint;
    const d0 = getDistance(x0, y0);
    if (!isInside(d0, innerRadius, outerRadius)) {
        //consecutive points outside
        return undefined;
    }
    const helper = new HelperLine(originalPoint, new Point(x0, y0));
    const outerCircle = new HelperCircle(new Point(0, 0), outerRadius);
    const innerCircle = new HelperCircle(new Point(0, 0), innerRadius);
    let intersections = [];
    if (d > outerRadius) {
        intersections = helper.intersect(outerCircle);
    } else {
        intersections = helper.intersect(innerCircle);
    }
    if (
        //round to prevent strange behaviour near origin
        //@todo fix this
        Math.sign(Math.round(originalPoint.x)) === Math.sign(intersections[0].x) &&
        Math.sign(Math.round(originalPoint.y)) === Math.sign(intersections[0].y)
    ) {
        return [intersections[0].x, intersections[0].y];
    } else {
        return [intersections[1].x, intersections[1].y];
    }
}

function getDistance(x: number, y: number) {
    return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
}

function isInside(d: number, innerRadius: number, outerRadius: number) {
    return d <= outerRadius && d >= innerRadius;
}

function getTerritoryColor(owner: number, state: State) {
    const color = getColor(owner, state);
    const rgba = Konva.Util.colorToRGBA(color);
    if (!rgba) {
        console.log(owner, color);
        return 'pink';
    }
    rgba.a = 0.5;

    return 'rgba(' + Object.values(rgba).join(',') + ')';
}
