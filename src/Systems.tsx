import React from 'react';
import { Circle, Group, Line, Star, Text } from 'react-konva';

import { getColor, getSystem } from './helpers';
import { useConfig } from './hooks/useConfig';
import { useGalaxy } from './hooks/useGalaxy';

export function Systems() {
    const [state, dispatchAction] = useGalaxy();
    const [config, configAction] = useConfig();

    function getSystemColor(id: string, owner: number | undefined) {
        if (owner === undefined) {
            return 'white';
        }
        if (config.spoilerFree) {
            let surveyed: number[] = [];
            if (config.activePlayer !== undefined) {
                const country = state.countries[config.activePlayer];
                surveyed = country.visited_objects || [];
                // if (owner === undefined && surveyed.includes(Number(id))) {
                //     return 'grey';
                // }
            }

            if (owner !== config.activePlayer && !surveyed.includes(Number(id))) {
                return 'white';
            }
        }
        return getColor(owner, state);
    }
    return (
        <>
            {Object.entries(state.hyperlanes).map(([id, hyperlane]) => {
                const from = getSystem(hyperlane.systems[0], state);
                const to = getSystem(hyperlane.systems[1], state);
                return (
                    <Line
                        key={id}
                        points={[from.coordinate.x, from.coordinate.y, to.coordinate.x, to.coordinate.y]}
                        stroke={config.hyperlaneColor}
                    />
                );
            })}
            {Object.entries(state.systems).map(([id, system]) => {
                return (
                    <Group
                        key={id}
                        x={system.coordinate.x}
                        y={system.coordinate.y}
                        draggable={config.draggableStars}
                        onDragMove={event => dispatchAction.moveSystem({ ...event, id: parseInt(id) })}
                    >
                        <Circle
                            radius={10}
                            onClick={() => {
                                configAction.setSelectedObject(system);
                            }}
                        />
                        <Star fill={getSystemColor(id, system.owner)} numPoints={6} innerRadius={2} outerRadius={5} />
                        {config.displayStarNames && (
                            <Text
                                fontSize={config.systemFontSize}
                                x={10}
                                y={-10}
                                text={system.name}
                                scale={{ x: 1 / config.scale, y: 1 / config.scale }}
                            ></Text>
                        )}
                    </Group>
                );
            })}
        </>
    );
}
