import { Delaunay } from 'd3-delaunay';

import { State } from './hooks/useGalaxy';

export function getEmpires(state: State) {
    const points: number[][] = [];
    for (const system of Object.values(state.systems)) {
        points.push([system.coordinate.x, system.coordinate.y]);
    }
    const delaunay = Delaunay.from(points);
    const v = delaunay.voronoi();
    v.xmax = state.galaxy.outerRadius + 10;
    v.xmin = -state.galaxy.outerRadius - 10;
    v.ymax = state.galaxy.outerRadius + 10;
    v.ymin = -state.galaxy.outerRadius - 10;
    return v;
}
