import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Button,
    Checkbox,
    FormControl,
    FormControlLabel,
    FormGroup,
    Input,
    InputLabel,
    Link,
    makeStyles,
    MenuItem,
    Select,
    Typography
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import ColorPicker from 'material-ui-color-picker';
import React from 'react';
import { Stage } from 'react-konva';

import { useConfig } from '../hooks/useConfig';
import { useExampleSave, useGalaxy } from '../hooks/useGalaxy';

import { Loader } from './Loader';
import { Navigation } from './Navigation';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    footer: {
        bottom: 8,
        position: 'absolute',
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    panel: {
        flexDirection: 'column',
        '& > *': {
            margin: theme.spacing(1),
        },
    },
}));

function BoolOption({
    label,
    value,
    setter,
    disabled = false,
}: {
    label: string;
    value: boolean;
    setter: (v: boolean) => void;
    disabled?: boolean;
}) {
    return (
        <FormControlLabel
            control={
                <Checkbox
                    checked={value}
                    onChange={(event) => setter(event.currentTarget.checked)}
                    disabled={disabled}
                />
            }
            label={label}
        />
    );
}

function download(stage: React.RefObject<Stage>) {
    const link = document.createElement('a');
    link.download = 'stellaris-map.jpg';
    if (!stage.current) {
        return;
    }
    const actual = stage.current.getStage();
    link.href = actual.toDataURL({ mimeType: 'image/jpeg' }) || '';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}

export function Sidebar({ stage }: { stage: React.RefObject<Stage> }) {
    const [config, configAction] = useConfig();
    const [{ players }] = useGalaxy();
    const classes = useStyles();
    const setExample = useExampleSave();
    return (
        <>
            <Accordion>
                <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="load-content">
                    Load / Download
                </AccordionSummary>
                <AccordionDetails className={classes.panel}>
                    <Loader />
                    <Button variant="contained" onClick={setExample}>
                        Load example
                    </Button>
                    <Button variant="contained" onClick={() => download(stage)}>
                        Download image
                    </Button>
                </AccordionDetails>
            </Accordion>
            <Accordion>
                <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="zoom-content">
                    Navigation
                </AccordionSummary>
                <AccordionDetails className={classes.panel}>
                    <Navigation stage={stage} />
                </AccordionDetails>
            </Accordion>
            <Accordion>
                <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="global-options-content">
                    Global options
                </AccordionSummary>
                <AccordionDetails className={classes.panel}>
                    <FormGroup>
                        <BoolOption
                            label="Display helper lines"
                            value={config.displayHelpers}
                            setter={configAction.setDisplayHelpers}
                        />

                        <BoolOption
                            label="Movable stars"
                            value={config.draggableStars}
                            setter={configAction.setStarsDraggable}
                        />
                        <BoolOption
                            label="Spoiler free"
                            value={config.spoilerFree}
                            setter={configAction.setSpoilerFree}
                        />
                    </FormGroup>
                    <FormControl className={classes.formControl}>
                        <InputLabel id="active-player-select-label">Active Player</InputLabel>
                        <Select
                            labelId="active-player-select-label"
                            value={config.activePlayer !== undefined ? config.activePlayer : ''}
                            onChange={(event) => configAction.setActivePlayer(Number(event.target.value))}
                        >
                            {players.map((player) => (
                                <MenuItem value={player.country} key={player.country}>
                                    {player.name}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </AccordionDetails>
            </Accordion>
            <Accordion>
                <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="map-options-content">
                    Map options
                </AccordionSummary>
                <AccordionDetails className={classes.panel}>
                    <BoolOption
                        label="Display star names"
                        value={config.displayStarNames}
                        setter={configAction.setDisplayStarNames}
                    />
                    <ColorPicker
                        value={config.hyperlaneColor}
                        label="Hyperlane color"
                        onChange={(color: string) => configAction.setHyperlaneColor(color)}
                    />
                    <InputLabel>
                        Font size system names
                        <Input
                            type="number"
                            value={config.systemFontSize}
                            onChange={(event) => configAction.setSystemFontSize(Number(event.target.value))}
                        />
                    </InputLabel>
                </AccordionDetails>
            </Accordion>
            <Accordion defaultExpanded>
                <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="selected-object-content">
                    Selected object
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        {config.selectedObject
                            ? config.selectedObject.name
                            : 'click on a system or empire to select it'}
                    </Typography>
                </AccordionDetails>
            </Accordion>

            <div className={classes.footer}>
                <Typography>
                    <Link href="https://gitlab.com/Khartir/stellaris-map-viewer/issues" target="blank">
                        Report Issues
                    </Link>
                </Typography>
            </div>
        </>
    );
}
