import { Button, Slider, Typography } from '@material-ui/core';

import React from 'react';
import { Stage } from 'react-konva';

import { useZoom } from '../helpers';
import { useConfig } from '../hooks/useConfig';

export function Navigation({ stage }: { stage: React.RefObject<Stage> }) {
    const [config, configAction] = useConfig();
    const zoom = useZoom(stage);
    function zoomHandler(scale: number) {
        if (!stage.current) {
            return;
        }
        const actual = stage.current.getStage();
        const currentCenter = {
            x: (-config.offset.x - actual.x()) / config.scale,
            y: (-config.offset.y - actual.y()) / config.scale
        };
        zoom(scale, currentCenter);
    }
    return (
        <>
            <Typography id="zoom-slider" gutterBottom>
                Zoom
            </Typography>
            <Slider
                value={config.scale}
                onChange={(_, newValue) => zoomHandler(Array.isArray(newValue) ? newValue[0] : newValue)}
                min={0.1}
                max={16}
                aria-labelledby="zoom-slider"
            />
            <Button
                variant="contained"
                onClick={() => {
                    if (stage.current) {
                        stage.current.getStage().position({ x: 0, y: 0 });
                        configAction.setScale(1);
                    }
                }}
            >
                Reset zoom
            </Button>
        </>
    );
}
