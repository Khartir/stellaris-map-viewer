import { Button, CircularProgress, makeStyles } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';

import React, { ChangeEvent, useCallback, useEffect, useState } from 'react';
// eslint-disable-next-line import/no-webpack-loader-syntax
import Worker from 'worker-loader!../worker/loader';

import { useSetGalaxy } from '../hooks/useGalaxy';

const useStyles = makeStyles((theme) => ({
    wrapper: {
        position: 'relative',
    },
    buttonProgress: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
    },
}));

export function Loader() {
    const { decode, loading, error } = useWorker();
    const classes = useStyles();
    function handleFile(event: ChangeEvent<HTMLInputElement>) {
        if (!event.target.files) {
            return;
        }
        decode(event.target.files[0]);
    }
    return (
        <div className={classes.wrapper}>
            <Button variant="contained" color="primary" component="label" disabled={loading}>
                Upload File
                <input type="file" style={{ display: 'none' }} onChange={handleFile} />
            </Button>
            {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
            {error && (
                <Alert severity="error">
                    <AlertTitle>Error</AlertTitle>
                    {error.message}
                </Alert>
            )}
        </div>
    );
}

function useWorker() {
    const setGalaxy = useSetGalaxy();
    const [worker, setWorker] = useState<Worker | undefined>(undefined);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState<null | Error>(null);
    useEffect(() => {
        const worker = new Worker();
        worker.onmessage = (event: MessageEvent) => {
            if ('success' === event.data.status) {
                setGalaxy(event.data.result);
            } else {
                setError(event.data.result);
            }
            setLoading(false);
        };
        worker.onerror = (event: ErrorEvent) => {
            console.log(event);
            setLoading(false);
        };
        setWorker(worker);
        return () => {
            worker.terminate();
            setLoading(false);
            setWorker(undefined);
        };
    }, [setGalaxy]);

    const decode = useCallback(
        (data) => {
            if (!worker) {
                throw Error('Worker not yet defined');
            }
            setLoading(true);
            setError(null);
            worker.postMessage(data);
        },
        [setLoading, worker]
    );

    return { decode, loading, error };
}
