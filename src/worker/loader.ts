import { parse } from 'jomini';
import { loadAsync } from 'jszip';

import { State } from '../hooks/useGalaxy';

// eslint-disable-next-line no-restricted-globals
addEventListener('message', (event) => {
    loadAsync(event.data)
        .then((zip) => {
            let valid = false;
            zip.forEach((relativePath, zipEntry) => {
                if (relativePath !== 'gamestate') {
                    return;
                }
                valid = true;
                zipEntry
                    .async('text')
                    .then((string) => {
                        console.log('unzipped');
                        const parsed = parse<RawState>(string);
                        // it seems this is an incrementing version
                        // 2.6.0 would probably work too
                        if (parsed.version_control_revision < 56796) {
                            throw Error(
                                `Stellaris map viewer is incompatible with this save game version (${parsed.version_control_revision} aka ${parsed.version}), Version 2.6.1 required`
                            );
                        }
                        postMessage({ status: 'success', result: parseRaw(parsed) });
                    })
                    .catch((e) => {
                        postMessage({ status: 'error', result: e });
                    });
            });
            if (!valid) {
                throw Error('Not a valid savegame');
            }
        })
        .catch((e) => {
            postMessage({ status: 'error', result: e });
        });
});

function parseRaw({ country, galactic_object, galaxy, galaxy_radius, starbase_mgr: { starbases }, player }: RawState) {
    console.log('parsed file');
    const parsed: State = {
        systems: {},
        countries: {},
        hyperlanes: {},
        galaxy: { outerRadius: galaxy_radius, innerRadius: galaxy.core_radius },
        players: player,
    };

    for (const [
        index,
        {
            coordinate: { x, y },
            type,
            name,
            hyperlane,
            starbase,
        },
    ] of Object.entries(galactic_object)) {
        const hyperlanes = [];
        if (Array.isArray(hyperlane)) {
            for (const lane of hyperlane) {
                const systems = [lane.to, parseInt(index)];
                systems.sort();
                parsed.hyperlanes[systems.join('-')] = { systems, length: lane.length };
                hyperlanes.push(systems.join('-'));
            }
        }
        const starbaseObject = starbases[starbase];
        let owner: number | undefined;
        if (!starbaseObject) {
            // no owner
            if (starbase !== 0xffffffff) {
                console.log('Missing starbase', {
                    coordinate: { x, y },
                    type,
                    name,
                    hyperlane,
                    starbase,
                });
            }
            owner = undefined;
        } else {
            owner = starbaseObject.owner;
        }
        parsed.systems[index] = {
            coordinate:
                //convert stellaris to canvas coords
                { x: -x, y },
            type,
            name,
            hyperlanes,
            owner,
        };
    }
    console.log('parsed galaxy');
    for (const [index, { flag, name, starting_system, terra_incognita, visited_objects, surveyed }] of Object.entries(
        country
    )) {
        if (!flag) {
            continue;
        }
        parsed.countries[index] = {
            color: flag.colors[0],
            name,
            starting_system,
            terra_incognita,
            visited_objects,
            surveyed,
        };
    }
    console.log('parsed empires');
    return parsed;
}

interface RawState {
    version: string;
    version_control_revision: number;
    country: {
        [index: string]: {
            flag: {
                colors: string[];
            };
            name: string;
            terra_incognita: { size: number; data: number[]; systems: number[] };
            visited_objects: number[];
            surveyed?: number[];
            starting_system?: number;
        };
    };
    galactic_object: {
        [index: string]: {
            coordinate: { x: number; y: number };
            type: string;
            name: string;
            hyperlane: {};
            starbase: number;
        };
    };
    galaxy: {
        core_radius: number;
    };
    galaxy_radius: number;
    starbase_mgr: {
        starbases: {
            system: string;
            owner: number;
        }[];
    };
    player: { name: string; country: number }[];
}
