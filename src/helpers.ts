import { RefObject, useCallback } from 'react';
import { Stage } from 'react-konva';

import { useConfig } from './hooks/useConfig';
import { State, useGalaxy } from './hooks/useGalaxy';

const colorTable = {
    dark_brown: '#8b7355',
    burgundy: '#8b0a50',
    dark_blue: '#104e8b',
    green: '#00cc00',
    brown: '#f4a460',
    pink: '#ff1493',
    blue: '#00b2ee',
    dark_green: '#008b00',
    orange: '#ff8c00',
    purple: '#9370db',
    turquoise: '#40E0D0',
    grey: '#9e9e9e',
    red_orange: '#ee4000',
    dark_purple: '#7d26cd',
    teal: '#008080',
    dark_grey: '#636363',
    red: '#ee0000',
    indigo: '#4B0082',
    dark_teal: '#0E302F',
    black: '#000000',
    //@todo
    light_green: 'green'
};
export function getColor(owner: number, state: State) {
    const label = getColorLabel(owner, state);
    if (Object.keys(colorTable).includes(label)) {
        //@ts-ignore
        return colorTable[label] as string;
    }

    return label;
}
function getColorLabel(owner: number, state: State) {
    const result = state.countries[owner];
    if (result) {
        return result.color;
    }
    return 'white';
}
export function getSystem(id: number, state: State) {
    const result = state.systems[id];
    if (result) {
        return result;
    }
    throw new Error('unknown system: ' + id);
}

export function useGetCountry() {
    const [galaxy] = useGalaxy();
    return useCallback(
        (owner: number) => {
            return galaxy.countries[owner];
        },
        [galaxy.countries]
    );
}

export function useZoom(stage: RefObject<Stage>) {
    const [config, configAction] = useConfig();
    return function zoom(newScale: number, center: { x: number; y: number }) {
        if (!stage.current) {
            return;
        }
        newScale = Math.min(newScale, 16);
        newScale = Math.max(newScale, 0.1);
        const actual = stage.current.getStage();
        const mousePointTo = {
            x: (center.x - actual.x()) / config.scale,
            y: (center.y - actual.y()) / config.scale
        };
        configAction.setScale(newScale);

        const newPos = {
            x: -(mousePointTo.x - center.x / newScale) * newScale,
            y: -(mousePointTo.y - center.y / newScale) * newScale
        };
        actual.position(newPos);
    };
}
