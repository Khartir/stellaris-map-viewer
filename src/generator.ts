import Konva from 'konva';

// function to generate a list of "targets" (circles)
function generateTargets(number = 10, owners: Collection<Owner>) {
    const result: Collection<System> = {};
    let c = 0;
    while (c++ < number) {
        result[c] = {
            id: c,
            x: 500 * Math.random() - 250,
            y: 500 * Math.random() - 250,
            owner: (owners[Math.floor(Math.random() * Object.keys(owners).length + 1)] as Owner).id,
            hyperlanes: []
        };
    }
    return result;
}

// function to generate arrows between targets
function generateConnectors(targets: Collection<System>, number = 10) {
    const result: Collection<Hyperlane> = {};
    let c = 0;
    while (c++ < number) {
        var from = targets[Math.floor(Math.random() * Object.keys(targets).length + 1)] as System;
        var to = targets[Math.floor(Math.random() * Object.keys(targets).length + 1)] as System;
        if (from === to) {
            continue;
        }
        const lane = {
            id: c,
            from: from.id,
            to: to.id
        };

        result[c] = lane;
        from.hyperlanes.push(lane);
        to.hyperlanes.push(lane);
    }
    return result;
}

function generateOwners() {
    const number = 2;
    const result: Collection<Owner> = {};
    let c = 0;
    while (c++ < number) {
        result[c] = { id: c, color: Konva.Util.getRandomColor() };
    }

    return result;
}

export const Generator = (count: number) => {
    const owners = generateOwners();
    const targets = generateTargets(count, owners);
    // return {
    //     systems: targets,
    //     hyperlanes: generateConnectors(targets, count),
    //     owners
    // } as State;
    return {
        systems: {
            '1': {
                id: 1,
                x: 111.49125204141933,
                y: -148.99952279698655,
                owner: 1,
                hyperlanes: [{ id: 3, from: 9, to: 1 }, { id: 4, from: 10, to: 1 }, { id: 6, from: 9, to: 1 }]
            },
            '2': {
                id: 2,
                x: 60.44746797282551,
                y: 180.55318177290002,
                owner: 2,
                hyperlanes: [{ id: 7, from: 2, to: 3 }, { id: 8, from: 2, to: 8 }, { id: 10, from: 6, to: 2 }]
            },
            '3': {
                id: 3,
                x: 19.97033880502704,
                y: -2.6907997614015926,
                owner: 1,
                hyperlanes: [{ id: 1, from: 9, to: 3 }, { id: 7, from: 2, to: 3 }]
            },
            '4': { id: 4, x: -224.4661954665708, y: 231.7048479312022, owner: 2, hyperlanes: [] },
            '5': { id: 5, x: -376.49026615387675, y: 42.20124763488067, owner: 2, hyperlanes: [] },
            '6': {
                id: 6,
                x: -184.4177281620478,
                y: 57.632854015558905,
                owner: 2,
                hyperlanes: [{ id: 2, from: 6, to: 9 }, { id: 10, from: 6, to: 2 }]
            },
            '7': {
                id: 7,
                x: 363.5936290027903,
                y: 59.79513741436082,
                owner: 1,
                hyperlanes: [{ id: 5, from: 8, to: 7 }]
            },
            '8': {
                id: 8,
                x: 191.67574022177757,
                y: 26.50710589503521,
                owner: 1,
                hyperlanes: [{ id: 5, from: 8, to: 7 }, { id: 8, from: 2, to: 8 }, { id: 9, from: 8, to: 10 }]
            },
            '9': {
                id: 9,
                x: -279.3762353150992,
                y: -128.36017767558045,
                owner: 2,
                hyperlanes: [
                    { id: 1, from: 9, to: 3 },
                    { id: 2, from: 6, to: 9 },
                    { id: 3, from: 9, to: 1 },
                    { id: 6, from: 9, to: 1 }
                ]
            },
            '10': {
                id: 10,
                x: 301.7247829193869,
                y: -139.95705597346898,
                owner: 1,
                hyperlanes: [{ id: 4, from: 10, to: 1 }, { id: 9, from: 8, to: 10 }]
            }
        },
        hyperlanes: {
            '1': { id: 1, from: 9, to: 3 },
            '2': { id: 2, from: 6, to: 9 },
            '3': { id: 3, from: 9, to: 1 },
            '4': { id: 4, from: 10, to: 1 },
            '5': { id: 5, from: 8, to: 7 },
            '6': { id: 6, from: 9, to: 1 },
            '7': { id: 7, from: 2, to: 3 },
            '8': { id: 8, from: 2, to: 8 },
            '9': { id: 9, from: 8, to: 10 },
            '10': { id: 10, from: 6, to: 2 }
        },
        owners: { '1': { id: 1, color: '#11fbec' }, '2': { id: 2, color: '#efd751' } }
    } as State;
};

interface Owner {
    id: number;
    color: string;
}

export interface System {
    id: number;
    x: number;
    y: number;
    owner: number;
    hyperlanes: Hyperlane[];
}

export interface Hyperlane {
    id: number;
    from: number;
    to: number;
}
export interface State {
    systems: Collection<System>;
    owners: Collection<Owner>;
    hyperlanes: Collection<Hyperlane>;
}
export interface Collection<T> {
    [index: number]: T;
}
